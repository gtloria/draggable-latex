var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/node_modules'));
app.get('/', function(req, res, next) {  
    res.sendFile(__dirname + '/public/index.html');
});

io.on('connection', function(client) {  
    console.log('Client connected...');
    //var spawn = require('child_process').spawn;
    //var child = spawn('java', ['-jar', 'z-0.1.0-SNAPSHOT-standalone.jar']);
    //child.stdout.on('data', function (data) {console.log('' + data);})
    client.on('join', function(data) {
        console.log(data)
	client.emit('messages', 'Hello from the server') })
    client.on('move', function(data) {
	console.log(data.ID, "Sprite moved:", data.x, data.y)})
    client.on('new', function(data) {
	console.log('message: ', 'new ' + data.input + '\n')
	var execFile = require('child_process').execFile
	var pngc = execFile('./png_script.sh', [data.input, data.filename])
	    .on('exit', code => client.emit('image_made', '')) })
	//child.stdin.write('new ' + data + '\n')})
    client.on('disconnect', function() {
	console.log('Client disconnected...') })
	//child.kill()})
});

var port = 8080;
server.listen(port, function() {
    console.log('server listening on port ' + port);
});
