
var socket = io.connect('http://127.0.0.1:8080');
socket.on('connect', function(data) {
    socket.emit('join', 'Hello World from client');
});

var app = new PIXI.Application(600, 400, {backgroundColor : 0xEEEEEE});
document.body.appendChild(app.view);

app.stage.interactive = true;
app.stage.hitArea = new PIXI.Rectangle(0, 0, 800, 600);
app.stage.on('click', tapHandler);

var spriteCount = 0;

function tapHandler(event) {
    if(!this[tappedTwice]) {
        this[tappedTwice] = true;
        setTimeout( () => { this[tappedTwice] = false; }, 300 );
        return;
    }
    //action on double tap goes below
    var x = app.renderer.plugins.interaction.mouse.global.x;
    var y = app.renderer.plugins.interaction.mouse.global.y;
    var inputValue = ""
    swal({
	title: "Enter an expression",
	text: "this includes constants and variables",
	type: "input",
	showCancelButton: true,
	closeOnConfirm: true,
	inputPlaceholder: "Enter something"
    },
	 function(inputValue){
	     var input = inputValue.replace(/\s/g, "");
	     if (input === false) return false;
	     if (input === "") {
		 swal.showInputError("You need to write something!");
		 return false
	     }
	     
	     var spriteFile = spriteFilename(input);
	     socket.emit('new', {input: input, filename: spriteFile});
	     socket.once('image_made', function(data) {
		 var sprite = createSprite(x, y, spriteFile);
		 console.log('sprite ID is ', sprite.ID);})
	 })
}

function createSprite(x, y, spriteFile) {
    var sprite = PIXI.Sprite.fromImage(spriteFile + '.png');
    sprite.anchor.set(0.5);
    sprite.x = x;
    sprite.y = y;
    sprite.interactive = true;
    sprite.buttonMode = true;
    sprite
        .on('pointerdown', onDragStart)
        .on('pointerup', onDragEnd)
        .on('pointerupoutside', onDragEnd)
        .on('pointermove', onDragMove);

    app.stage.addChild(sprite);
    sprite.ID = spriteCount++;
    
    return sprite;
}

const tappedTwice = Symbol('tappedTwice');

function onDragStart(event) {
    // store a reference to the data
    // the reason for this is because of multitouch
    // we want to track the movement of this particular touch
    this.data = event.data;
    this.alpha = 0.5;
    this.dragging = true;
}

function onDragEnd() {
    this.alpha = 1;
    this.dragging = false;
    // set the interaction data to null
    this.data = null;
    socket.emit('move', {ID: this.ID, x: this.x, y: this.y});
}

function onDragMove() {
    if (this.dragging) {
        var newPosition = this.data.getLocalPosition(this.parent);
        this.x = newPosition.x;
        this.y = newPosition.y;
    }
}

function spriteFilename(input) {
    return input.replace(/\\/g, "backslash")
	.replace(/\//g, "slash")
	.replace(/%/g,  "percent")
	.replace(/\*/g, "star")
	.replace(/\:/g, "colon")
	.replace(/\|/g, "pipe")
	.replace(/\"/g, "quote")
	.replace(/\</g, "lessthan")
	.replace(/\>/g, "greaterthan")
	.replace(/\./g, "dot")
}
