#! /bin/bash
if [ ! -f ./public/$2.png ]; then
    echo '%&preamble' > $2.tex
    echo '\begin{document}' >> $2.tex
    echo "\$$1\$" >> $2.tex
    echo '\end{document}' >> $2.tex
    pdflatex $2.tex
    rm -f $2.aux $2.log $2.tex
    convert -density 300 $2.pdf -quality 90 $2.png
    rm -f $2.pdf
    mv $2.png public/
fi
